﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using banking.Models;
namespace banking.Abstract
{
    public interface ITaiKhoanRepository
    {
        bool checkLogin(TaiKhoan e);
        List<GiaoDich> LichSuGiaoDich(string maKhachHang);
        string layMaKhachHang(string soTaiKhoan);
        decimal XemSoDuTaiKhoan(string maKhachHang);
        List<TaiKhoan> XemThongTinTaiKhoan(string maKhachHang);
        bool CapNhatMatKhau(string maKhachHang, string matKhau, string matKhauMoi);

    }
}