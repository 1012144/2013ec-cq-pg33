﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using banking.Abstract;
using System.Data.SqlClient;
using System.Data;
using banking.SQL;
using banking.Models;
using banking.Scripto;
using System.Web.Mvc;
namespace banking.Concrete
{
    public class TaiKhoanRepository : ITaiKhoanRepository
    {
        SqlCommand cmd = null;
        // DataTable dt = null;
        SqlConnectionData connection = null;
        public bool checkLogin(TaiKhoan taiKhoan)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_CHECK_LOGIN";

            byte[] hashMatKhau = HashSHA1.hash(taiKhoan.matKhau);

            cmd.Parameters.Add("@soTaiKhoan", SqlDbType.VarChar).Value = taiKhoan.soTaiKhoan;
            cmd.Parameters.Add("@tenDangNhap", SqlDbType.VarChar).Value = taiKhoan.tenDangNhap;
            cmd.Parameters.Add("@matKhau", SqlDbType.VarBinary).Value = hashMatKhau;

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows == true)
            {
                return true;
            }
            //
            return false;
        }




        public List<GiaoDich> LichSuGiaoDich(string maKhachHang)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_LICHSU_GIAODICH";
            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = maKhachHang;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;

            DataTable table = new DataTable();
            table.Clear();
            da.Fill(table);

            List<GiaoDich> listGiaoDich = new List<GiaoDich>();
            int numRows = table.Rows.Count;
            for (int i = 0; i < numRows; i++)
            {
                GiaoDich gd = new GiaoDich
                {
                    giaTriGiaoDich = Int32.Parse(table.Rows[i]["giaTriGiaoDich"].ToString()),
                    loaiGiaoDich = table.Rows[i]["loaiGiaDich"].ToString(),
                    maBieuPhi = table.Rows[i]["maBieuPhi"].ToString(),
                    maGiaoDich = table.Rows[i]["maGiaoDich"].ToString(),
                    nganHangGui = table.Rows[i]["nganHangGui"].ToString(),
                    nganHangNhan = table.Rows[i]["nganHangNhan"].ToString(),
                    nguoiGui = table.Rows[i]["nguoiGui"].ToString(),
                    nguoiNhan = table.Rows[i]["nguoiNhan"].ToString(),
                    phiGiaoDich = Int32.Parse(table.Rows[i]["phiGiaoDich"].ToString()),
                    soTaiKhoanGui = table.Rows[i]["soTaiKhoanGui"].ToString(),
                    soTaiKhoanNhan = table.Rows[i]["soTaiKhoanNhan"].ToString(),
                    thoiGianGiaoDich = DateTime.Parse(table.Rows[i]["thoiGianGiaoDich"].ToString())
                };

                listGiaoDich.Add(gd);
            }
            return listGiaoDich;
        }


        public string layMaKhachHang(string soTaiKhoan)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_LAY_MAKHACHHANG";

            cmd.Parameters.Add("@soTaiKhoan", SqlDbType.VarChar).Value = soTaiKhoan;


            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows == true)
            {
                reader.Read();
                string s = reader.GetString(0);
                return s;
            }
            //
            return null;
        }


        public decimal XemSoDuTaiKhoan(string maKhachHang)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_XEMSODU_TAIKHOAN";

            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = maKhachHang;


            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows == true)
            {
                // value.
                reader.Read();
                decimal a = Int32.Parse(reader.GetValue(0).ToString());
                return a;

            }
            //
            return 0;
        }


        public List<TaiKhoan> XemThongTinTaiKhoan(string maKhachHang)
        {

            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_THONGTIN_TAIKHOAN";

            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = maKhachHang;
            DataTable table = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            da.Fill(table);

            //set value to TaiKhoan
            List<TaiKhoan> list = new List<TaiKhoan>();
            for (int i = 0; i < table.Rows.Count; i++)
            {
                TaiKhoan taikhoan = new TaiKhoan
                {
                    soTaiKhoan = table.Rows[i]["soTaiKhoan"].ToString(),
                    maKhachHang = table.Rows[i]["maKhachHang"].ToString(),

                    soDu = Int32.Parse(table.Rows[i]["soDu"].ToString()),

                    trangThai = table.Rows[i]["trangThai"].ToString(),
                    //ngayKichHoat = DateTime.Parse(table.Rows[0]["ngayKichHoat"].ToString()),
                    maPin = "",
                    tenDangNhap = table.Rows[i]["tenTaiKhoan"].ToString(),
                    matKhau = "",
                    loaiTien = table.Rows[i]["loaiTien"].ToString()

                };
                list.Add(taikhoan);
            }
           
            return list;

        }


        public bool CapNhatMatKhau(string maKhachHang, string matKhau, string matKhauMoi)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "SP_CHANGE_PASSWORD";

            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = maKhachHang;
            cmd.Parameters.Add("@matKhau", SqlDbType.VarBinary).Value = HashSHA1.hash(matKhau);
            cmd.Parameters.Add("@matKhauMoi", SqlDbType.VarBinary).Value = HashSHA1.hash(matKhauMoi);
            int rowseffect=cmd.ExecuteNonQuery();
            if (rowseffect >0)
            {
                return true;
            }
            //DataTable table = new DataTable();

            return false;
        }
    }
}