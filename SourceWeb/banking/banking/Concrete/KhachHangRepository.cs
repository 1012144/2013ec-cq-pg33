﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using banking.Abstract;
using System.Data.SqlClient;
using System.Data;
using banking.Models;
using banking.SQL;
using banking.Scripto;

namespace banking.Concrete
{
    public class KhachHangRepository : IKhachHang
    {
        SqlCommand cmd = null;

        SqlConnectionData connection = null;
        public KhachHang layThongTinKhachHang(string maKhachHang)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandText = "SP_LAYTHONGTIN_KHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = maKhachHang;
            //
            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd;
            DataTable table = new DataTable();
            table.Clear();
            da.Fill(table);
            // set value to khachhang;
            KhachHang khachHang = new KhachHang();
            if (table != null)
            {
                khachHang.maKhachHang = table.Rows[0]["maKhachHang"].ToString();
                khachHang.tenKhachHang = table.Rows[0]["tenKhachHang"].ToString();
                khachHang.hoKhachHang = table.Rows[0]["hoKhachHang"].ToString();
                khachHang.tenDemKhachHang = table.Rows[0]["tenDemKhachHang"].ToString();
                khachHang.soDienThoai = table.Rows[0]["soDienThoai"].ToString();
                khachHang.diaChi_1 = table.Rows[0]["diaChi_1"].ToString();
                khachHang.diaChi_2 = table.Rows[0]["diaChi_2"].ToString();
                khachHang.loaiKhachHang = table.Rows[0]["loaiKhachHang"].ToString();
                khachHang.tenCongTy = table.Rows[0]["tenCongTy"].ToString();
                khachHang.ngheNghiep = table.Rows[0]["ngheNghiep"].ToString();
                khachHang.chucVu = table.Rows[0]["chucVu"].ToString();
                khachHang.mucLuong = Int32.Parse(table.Rows[0]["mucLuong"].ToString());
            }
            return khachHang;
        }

        public bool capNhapThongTinKhachHang(KhachHang khachhang, string matkhau)
        {
            cmd = new SqlCommand();
            connection = new SqlConnectionData();
            cmd.Connection = connection.GetConnection();
            cmd.CommandText = "SP_CAPNHAT_THONGTIN_KHACHHANG";
            cmd.CommandType = CommandType.StoredProcedure;

            //
            cmd.Parameters.Add("@maKhachHang", SqlDbType.VarChar).Value = khachhang.maKhachHang;
            cmd.Parameters.Add("@tenKhachHang", SqlDbType.VarChar).Value = khachhang.tenKhachHang;
            cmd.Parameters.Add("@hoKhachHang", SqlDbType.VarChar).Value = khachhang.hoKhachHang;
            cmd.Parameters.Add("@tenDemKhachHang", SqlDbType.VarChar).Value = khachhang.tenDemKhachHang;
            cmd.Parameters.Add("@soDienThoai", SqlDbType.VarChar).Value = khachhang.soDienThoai;
            cmd.Parameters.Add("@diaChi_1", SqlDbType.VarChar).Value = khachhang.diaChi_1;
            cmd.Parameters.Add("@diaChi_2", SqlDbType.VarChar).Value = khachhang.diaChi_2;
            cmd.Parameters.Add("@tenCongTy", SqlDbType.VarChar).Value = khachhang.tenCongTy;
            cmd.Parameters.Add("@ngheNghiep", SqlDbType.VarChar).Value = khachhang.ngheNghiep;
            cmd.Parameters.Add("@chucVu", SqlDbType.VarChar).Value = khachhang.chucVu;
            cmd.Parameters.Add("@mucLuong", SqlDbType.Int).Value = (Int32)khachhang.mucLuong;
            cmd.Parameters.Add("@matKhau", SqlDbType.VarBinary).Value = HashSHA1.hash(matkhau);
            int result =cmd.ExecuteNonQuery();
            if (result > 0)
            {
                return true;
            }
            return false;
        }
    }
}