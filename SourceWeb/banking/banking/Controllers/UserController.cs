﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using banking.Abstract;
using banking.Models;
using banking.Concrete;

namespace banking.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        private IKhachHang iKhachHangRepo;
        private ITaiKhoanRepository iTaiKhoanRepo;
        public UserController()
        {
            iTaiKhoanRepo = new TaiKhoanRepository();
            iKhachHangRepo = new KhachHangRepository();
        }
        public ActionResult Index()
        {

            string maKhachHang = Session["maKhachHang"].ToString();

            return View(iTaiKhoanRepo.XemThongTinTaiKhoan(maKhachHang));
        }
        public ActionResult ThongTinKhachHang()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            KhachHang khachhang = iKhachHangRepo.layThongTinKhachHang(maKhachHang);

            //khac
            return View(khachhang);
        }
        [HttpGet]
        public ActionResult CapNhatThongTinKhachHang()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            KhachHang khachhang = iKhachHangRepo.layThongTinKhachHang(maKhachHang);

            return View(khachhang);
        }
        [HttpPost]
        public ActionResult CapNhatThongTinKhachHang(KhachHang khachhang, string matKhau)
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            khachhang.maKhachHang = maKhachHang;
            bool result = iKhachHangRepo.capNhapThongTinKhachHang(khachhang, matKhau);
            if (result == true)
            {
                TempData["notify"] = "* Cập nhật thông tin thành công!";
                return RedirectToAction("ThongTinKhachHang");
            }
            else
            {
                TempData["notify"] = "Mật khẩu xác nhận không đúng.";
                return RedirectToAction("CapNhatThongTinKhachHang");
            }

        }
        public ActionResult HuyCapNhatThongTinTaiKhoan()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            TempData["notify"] = "* Đã hủy sửa đổi thông tin khách hàng.";
            return RedirectToAction("ThongTinKhachHang");
        }
        [HttpGet]
        public ActionResult CapNhatMatKhau()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            return View((object)maKhachHang);
        }

        [HttpPost]
        public ActionResult CapNhatMatKhau(string matKhau, string matKhauMoi)
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            bool result = iTaiKhoanRepo.CapNhatMatKhau(maKhachHang, matKhau, matKhauMoi);
           
            if (result)
            {
                TempData["notify"] = "* Thay đổi mật khẩu thành công!";
                return RedirectToAction("ThongTinKhachHang", new { maKhachHang = maKhachHang });
            }
            else
            {
                TempData["notify"] = "* Mật khẩu nhập vào không đúng.";
                return View("CapNhatMatKhau");
            }
            //return RedirectToAction("");
        }
        //
        public ActionResult XemLichSuGiaoDich()
        {
            //
            string maKhachHang = Session["maKhachHang"].ToString();
            //maKhachHang = "123456";
            //
            List<GiaoDich> danhSachGiaoDich = iTaiKhoanRepo.LichSuGiaoDich(maKhachHang);
            return View(danhSachGiaoDich);
        }
        //
        public ActionResult XemSoDuTaiKhoan()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            return View();
        }
        public ActionResult ThongTinTaiKhoan()
        {
            string maKhachHang = Session["maKhachHang"].ToString();
            return View(iTaiKhoanRepo.XemThongTinTaiKhoan(maKhachHang));
        }
        public ActionResult DangXuat()
        {
            Session["maKhachHang"]=null;
            return RedirectToAction("Index", "Home");
        }

        // taikhoan
        public ActionResult TaiKhoan()
        {
            ViewBag.Selected = "taikhoan";
            return View();
        }
        //giao dich
        public ActionResult GiaoDich()
        {
            return View();
        }
        // chuyen khoan trong he thong
        public PartialViewResult ChuyenKhoanTrongHeThong()
        {
            return PartialView( "_ChuyenKhoanTrongHeThongPartial",new GiaoDich());
        }
        [HttpPost]
        public ActionResult ChuyenKhoanTrongHeThong(GiaoDich giaodich)
        {
            if (ModelState.IsValid)
            {
                return PartialView("_XacNhanChuyenKhoanTrongheThongPartial", giaodich);
            }
            return PartialView("_ChuyenKhoanTrongHeThongPartial", giaodich);
        }

        //
       
    }

}
