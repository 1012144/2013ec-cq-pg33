﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CaptchaMvc.HtmlHelpers;
namespace banking.Controllers
{
    public class AccessControllController : Controller
    {
        //
        // GET: /AccessControll/
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string empty)
        {
            if (this.IsCaptchaValid("Captcha is not valid"))
            {
                TempData["Message"] = "Message: captcha is valid.";
                return RedirectToAction("Index", "Account");
            }

            TempData["ErrorMessage"] = "Error: captcha is not valid.";
            return View();
        }

    }
}
