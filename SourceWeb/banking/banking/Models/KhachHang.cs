﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace banking.Models
{
    public class KhachHang
    {
        [Display(Name="Mã khách hàng")]
        
        public string maKhachHang { get; set; }
        
        [Display(Name = "Tên khách hàng")]
        public string tenKhachHang { get; set; }

        [Display(Name = "Họ khách hàng")]
        public string hoKhachHang { get; set; }

        [Display(Name = "tên đệm khách hàng")]
        public string tenDemKhachHang { get; set; }

        [Display(Name = "số ĐT")]
        public string soDienThoai { get; set; }

        [Display(Name = "Địa chỉ 1")]
        public string  diaChi_1 { get; set; }

        [Display(Name = "Địa chỉ 2")]
        public string diaChi_2 { get; set; }

        [Display(Name = "Loại khách hàng")]
        public string loaiKhachHang { get; set; }

        [Display(Name = "Tên công ty")]
        public string tenCongTy { get; set; }

        [Display(Name = "Nghề nghiệp")]
        public string ngheNghiep { get; set; }

        [Display(Name = "Chức vụ")]
        public string chucVu { get; set; }

        [Display(Name = "Mức lương")]
        public decimal mucLuong { get; set; }
    }
}