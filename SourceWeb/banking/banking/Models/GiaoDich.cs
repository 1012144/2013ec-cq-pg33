﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace banking.Models
{
    public class GiaoDich
    {
        [Display(Name = "Mã Giao Dịch")]
        public string maGiaoDich { get; set; }

        [Display(Name = "Số Tài Khoản Gữi")]
        
        public string soTaiKhoanGui { get; set; }

        [Display(Name = "Số Tài Khoản Nhận")]
        public string soTaiKhoanNhan { get; set; }

        [Display(Name = "Mã Biểu Phí")]
        public string maBieuPhi { get; set; }

        [Display(Name = "Thời Gian Giao Dịch")]
        public DateTime thoiGianGiaoDich { get; set; }

        [Display(Name = "Giá Trị Giao Dịch")]
        public decimal giaTriGiaoDich { get; set; }

        [Display(Name = "Loại Giao Dịch")]
        public string loaiGiaoDich { get; set; }

        [Display(Name = "Người Gửi")]
        public string nguoiGui { get; set; }

        [Display(Name = "Người Nhận")]
        public string nguoiNhan { get; set; }

        [Display(Name = "Ngân Hàng Gữi")]
        public string nganHangGui { get; set; }

        [Display(Name = "Ngân Hàng Nhận")]
        public string nganHangNhan { get; set; }

        [Display(Name = "Phí Giao Dịch")]
        public decimal phiGiaoDich { get; set; }

        [Display(Name="Diễn giải")]
        public string dienGiai { get; set; }
    }
}