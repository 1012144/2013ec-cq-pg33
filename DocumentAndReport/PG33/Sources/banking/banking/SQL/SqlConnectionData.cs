﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
namespace banking.SQL
{
    public class SqlConnectionData
    {
        private SqlConnection con=new SqlConnection();
        private string connectionString;
        public void Open()
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
            con.ConnectionString = connectionString;
            con.Open();
            

        }
        public SqlConnection GetConnection()
        {
            if (con.State==ConnectionState.Closed)
            {
                Open();
            }
            return con;
        }
        public void disconnect(SqlConnection cons)
        {
            cons.Close();
            cons.Dispose();
        }
    }
}