﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using banking.Abstract;
using banking.Models;
using banking.Concrete;

namespace banking.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        private string maKhachHang;
        private IKhachHang iKhachHangRepo;
        private ITaiKhoanRepository iTaiKhoanRepo;
        public UserController()
        {
            maKhachHang = System.Web.HttpContext.Current.Session["maKhachHang"].ToString();
           // string a = Session["maKhachHang"].ToString();
            
            iTaiKhoanRepo = new TaiKhoanRepository();
            iKhachHangRepo = new KhachHangRepository();
        }
        public ActionResult Index()
        {

            return View(iTaiKhoanRepo.XemThongTinTaiKhoan(maKhachHang));
        }
        public PartialViewResult ThongTinKhachHang()
        {
          
            KhachHang khachhang = iKhachHangRepo.layThongTinKhachHang(maKhachHang);

            //khac
            return PartialView(khachhang);
        }
        [HttpGet]
        public PartialViewResult CapNhatThongTinKhachHang()
        {
            
            KhachHang khachhang = iKhachHangRepo.layThongTinKhachHang(maKhachHang);

            return PartialView(khachhang);
        }
        [HttpPost]
        public ActionResult CapNhatThongTinKhachHang(KhachHang khachhang, string matKhau)
        {
           
            khachhang.maKhachHang = maKhachHang;
            bool result = iKhachHangRepo.capNhapThongTinKhachHang(khachhang, matKhau);
            if (result == true)
            {
                TempData["notify"] = "* Cập nhật thông tin thành công!";
                return PartialView("ThongTinKhachHang");
            }
            else
            {
                TempData["notify"] = "Mật khẩu xác nhận không đúng.";
                return RedirectToAction("CapNhatThongTinKhachHang");
            }

        }
        public ActionResult HuyCapNhatThongTinTaiKhoan()
        {
           
            TempData["notify"] = "* Đã hủy sửa đổi thông tin khách hàng.";
            return RedirectToAction("ThongTinKhachHang");
        }

        [HttpGet]
        
        public PartialViewResult CapNhatMatKhau()
        {
            //return PartialView("_ChuyenKhoanTrongHeThongPartial", new GiaoDich());
            return PartialView("CapNhatMatKhauPartial");
        }

        [HttpPost]
        public ActionResult CapNhatMatKhau(string matKhau, string matKhauMoi, string matKhauMoi2)
        {
           
            bool result = iTaiKhoanRepo.CapNhatMatKhau(maKhachHang, matKhau, matKhauMoi);
           
            if (result)
            {
                TempData["notify"] = "* Thay đổi mật khẩu thành công!";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["notify"] = "* Mật khẩu nhập vào không đúng.";
                return PartialView("CapNhatMatKhauPartial");
            }
            //return RedirectToAction("");
        }
        //
        public ActionResult XemLichSuGiaoDich()
        {
            //
            
            //maKhachHang = "123456";
            //
            List<GiaoDich> danhSachGiaoDich = iTaiKhoanRepo.LichSuGiaoDich(maKhachHang);
            return View(danhSachGiaoDich);
        }
        //
        public ActionResult XemSoDuTaiKhoan()
        {
           
            return View();
        }
        public ActionResult ThongTinTaiKhoan()
        {
           
            return View(iTaiKhoanRepo.XemThongTinTaiKhoan(maKhachHang));
        }
        public ActionResult DangXuat()
        {
            Session["maKhachHang"]=null;
            return RedirectToAction("Index", "Home");
        }

        // taikhoan
        public ActionResult TaiKhoan()
        {
            ViewBag.Selected = "taikhoan";
            return View();
        }
        //giao dich
        public ActionResult GiaoDich()
        {
            return View();
        }
        // chuyen khoan trong he thong
        public PartialViewResult ChuyenKhoanTrongHeThong()
        {
            return PartialView( "_ChuyenKhoanTrongHeThongPartial",new GiaoDich());
        }
        [HttpPost]
        public ActionResult ChuyenKhoanTrongHeThong(GiaoDich giaodich)
        {
            if (ModelState.IsValid)
            {
                return PartialView("_XacNhanChuyenKhoanTrongheThongPartial", giaodich);
            }
            return PartialView("_ChuyenKhoanTrongHeThongPartial", giaodich);
        }

        //Ho so ca nhan
        public ViewResult HoSoCaNhan()
        {
            return View();
        }
       
    }

}
