﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using banking.Abstract;
using banking.Models;
using banking.Concrete;
namespace banking.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        ITaiKhoanRepository taiKhoanRepo;
        public AccountController()
        {
            taiKhoanRepo = new TaiKhoanRepository();
        }
        public ActionResult Index()
        {
            
            return View();
        }
        [HttpPost]
        public ActionResult Login(string tenTaiKhoan, string matKhau, string soTaiKhoan)
        {
            TaiKhoan e = new TaiKhoan { tenDangNhap = tenTaiKhoan, matKhau = matKhau, soTaiKhoan = soTaiKhoan };
            bool check = taiKhoanRepo.checkLogin(e);
            if (check)
            {
                Session["User"] = tenTaiKhoan;
                Session["maKhachHang"] = taiKhoanRepo.layMaKhachHang(soTaiKhoan);
                return RedirectToAction("Index", "User");
            }
            TempData["notify"] = "Your input is incorrect!";
            return RedirectToAction("Index", "Account");
        }
    }
}
