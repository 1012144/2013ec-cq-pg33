﻿using System;
using System.Data;
using banking.SQL;
using System.Data.SqlClient;
namespace banking.Models
{
    public class TaiKhoan
    {
        public string soTaiKhoan { set; get; }
        public string  maKhachHang{set;get;}
        public decimal soDu { set; get; }
        public string trangThai{set;get;}
        public DateTime ngayKichHoat{set;get;}
        public string maPin{set;get;}
        public string matKhau { set; get; }
        public string tenDangNhap{set; get;}
        public string loaiTien { get; set; } 
    }

}