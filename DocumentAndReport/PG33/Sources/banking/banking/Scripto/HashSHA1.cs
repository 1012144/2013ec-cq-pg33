﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Data;
using System.Text;

namespace banking.Scripto
{
    public static class HashSHA1
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueHash">string value what you want to hash</param>
        /// <returns>return byte[] hash value</returns>
        public static byte[] hash(string valueHash)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(valueHash));
            //return Encoding.UTF8.GetString(hash);
            return hash;

        }
        public static byte[] hash(byte[] valueHash)
        {
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] hash = sha1.ComputeHash(valueHash);
            return hash;
           
        }
    }
}