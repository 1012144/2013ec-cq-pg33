﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using banking.Models;
namespace banking.Abstract
{
    public interface IKhachHang
    {
        KhachHang layThongTinKhachHang(string maKhachHang);
        bool capNhapThongTinKhachHang(KhachHang khachhang,string matkhau);

    }
}