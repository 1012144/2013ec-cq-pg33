USE [E_Banking]
GO
/****** Object:  StoredProcedure [dbo].[SP_CAPNHAT_THONGTIN_KHACHHANG]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CAPNHAT_THONGTIN_KHACHHANG]
	@maKhachHang varchar(12),
	@tenKhachHang varchar(10),
	@hoKhachHang varchar(10),
	@tenDemKhachHang varchar(20),
	@soDienThoai varchar(11),
	@diaChi_1 varchar(50),
	@diaChi_2 nchar(10),
	
	@tenCongTy varchar(40),
	@ngheNghiep varchar(30),
	@chucVu varchar(25),
	@mucLuong int,
	@matKhau varbinary(max)
as
	if(select tk.matKhauE_Banking from taiKhoan tk where tk.maKhachHang=@maKhachHang and tk.matKhauE_Banking=@matKhau)>0
		begin
			update dbo.khachHang
			set 
				tenKhachHang=@tenKhachHang,
				hoKhachHang=@hoKhachHang,
				tenDemKhachHang=@tenDemKhachHang,
				soDienThoai=@soDienThoai,
				diaChi_1=@diaChi_1,
				diaChi_2=@diaChi_2,
				tenCongTy=@tenCongTy,
				ngheNghiep = @ngheNghiep,
				chucVu=@chucVu,
				mucLuong=@mucLuong

			where maKhachHang = @maKhachHang
		end
GO
/****** Object:  StoredProcedure [dbo].[SP_CHANGE_PASSWORD]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SP_CHANGE_PASSWORD]
	@maKhachHang varchar(12),
	@matKhau varbinary(max),
	@matKhauMoi varbinary(max)

as
	if
		(select  distinct(tk.matKhauE_Banking)  from taiKhoan tk where tk.maKhachHang=@maKhachHang and tk.matKhauE_Banking=@matKhau)>0
		begin
			update taiKhoan 
			set 
			matKhauE_Banking=@matKhauMoi
			where maKhachHang=@maKhachHang
		end
		
GO
/****** Object:  StoredProcedure [dbo].[SP_CHECK_LOGIN]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_CHECK_LOGIN]
	@soTaiKhoan varchar(20),
	@tenDangNhap varchar(50),
	@matKhau varbinary(max)
as
begin
	select * from taiKhoan tk where tk.soTaiKhoan = @soTaiKhoan and tk.tenTaiKhoan=@tenDangNhap and tk.matKhauE_Banking=@matKhau
end
GO
/****** Object:  StoredProcedure [dbo].[SP_CHECK_PASSWORD_BY_MAKHACHHANG]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_CHECK_PASSWORD_BY_MAKHACHHANG]
	@maKhachHang varchar(12),
	@matKhau varbinary(max)

as
	begin
		select * from taiKhoan tk where tk.maKhachHang=@maKhachHang and matKhauE_Banking=@matKhau
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_INS_THE]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_INS_THE]
	@SOTHE VARCHAR(20),
	@SOTAIKHOAN VARCHAR(20),
	@NGAYLAPTHE DATE,
	@NGAYHETHAN DATE,
	@TINHTRANGTHE CHAR(1),
	@LOAITHE CHAR(1),
	@DONVIPHATHANH VARCHAR(20)
AS
BEGIN
	DECLARE @ST VARBINARY(MAX)
	SET @ST = CONVERT(VARBINARY(MAX),@SOTHE,0)
	
	-- ENCRYPT DATA
	OPEN SYMMETRIC KEY Sym_password
	DECRYPTION BY CERTIFICATE Cert_Password WITH PASSWORD = '0912131';
	
	INSERT INTO The(soThe, soTaiKhoan, ngayLapThe, ngayHetHan, tinhTrangThe, loaiThe, donViPhatHanh)
		VALUES (ENCRYPTBYKEY(Key_GUID(N'Sym_password'),@SOTHE), @SOTAIKHOAN, @NGAYLAPTHE, @NGAYHETHAN, @TINHTRANGTHE, @LOAITHE, @DONVIPHATHANH)
			
	-- CLOSE SYM_KEY
	CLOSE SYMMETRIC KEY Sym_password;
	
END



GO
/****** Object:  StoredProcedure [dbo].[SP_LAY_MAKHACHHANG]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LAY_MAKHACHHANG]
	@soTaiKhoan varchar(20)
as
	begin
	 select maKhachHang from taiKhoan where soTaiKhoan=@soTaiKhoan
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_LAYTHONGTIN_KHACHHANG]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LAYTHONGTIN_KHACHHANG]
	@maKhachHang varchar(12)
as
begin
	select * from khachHang kh where kh.maKhachHang=@maKhachHang
end
GO
/****** Object:  StoredProcedure [dbo].[SP_LICHSU_GIAODICH]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_LICHSU_GIAODICH]
	@maKhachHang varchar(12)
as
	begin
		declare @soTaiKhoan varchar(20) set @soTaiKhoan =  'select soTaiKhoan from taiKhoan where maKhachHang=@maKhachHang';
		select * from giaoDich where soTaiKhoanGui = @soTaiKhoan or soTaiKhoanNhan=@soTaiKhoan
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_SOTHE]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_SEL_SOTHE]
AS
BEGIN
	OPEN SYMMETRIC KEY Sym_password
	DECRYPTION BY CERTIFICATE Cert_Password WITH PASSWORD = '0912131';
	SELECT THE.soTaiKhoan, CAST(DECRYPTBYKEY(THE.soThe) as VARCHAR(20)) AS SOTHE
	FROM The
	CLOSE SYMMETRIC KEY Sym_password;	
END



GO
/****** Object:  StoredProcedure [dbo].[SP_THONGTIN_TAIKHOAN]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_THONGTIN_TAIKHOAN]
	@maKhachHang varchar(12)
as
	begin
		select * from taiKhoan tk where tk.maKhachHang=@maKhachHang
	end

GO
/****** Object:  StoredProcedure [dbo].[SP_XEMSODU_TAIKHOAN]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SP_XEMSODU_TAIKHOAN]
	@maKhachHang varchar(12)
as
	begin
		select tk.soDu from taiKhoan tk where tk.maKhachHang=@maKhachHang
	end

GO
/****** Object:  Table [dbo].[bieuPhi]    Script Date: 12/21/2013 10:40:18 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bieuPhi](
	[maBieuPhi] [varchar](12) NOT NULL,
	[giaTriToiThieu] [int] NULL,
	[phiGiaoDich] [float] NULL,
	[maLoaiThe] [char](10) NULL,
 CONSTRAINT [PK_bieuPhi] PRIMARY KEY CLUSTERED 
(
	[maBieuPhi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[giaoDich]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[giaoDich](
	[maGiaoDich] [varchar](12) NOT NULL,
	[soTaiKhoanGui] [varchar](20) NULL,
	[soTaiKhoanNhan] [varchar](20) NULL,
	[maBieuPhi] [varchar](12) NULL,
	[thoiGianGiaoDich] [date] NULL,
	[giaTriGiaoDich] [int] NULL,
	[loaiGiaoDich] [varchar](20) NULL,
	[nguoiGui] [varchar](50) NULL,
	[nguoiNhan] [varchar](50) NULL,
	[nganHangGui] [varchar](50) NULL,
	[nganHangNhan] [varchar](50) NULL,
	[phiGiaoDich] [decimal](18, 0) NULL,
	[dienGiai] [text] NULL,
 CONSTRAINT [PK_giaoDich] PRIMARY KEY CLUSTERED 
(
	[maGiaoDich] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[khachHang]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[khachHang](
	[maKhachHang] [varchar](12) NOT NULL,
	[tenKhachHang] [varchar](10) NULL,
	[hoKhachHang] [varchar](10) NULL,
	[tenDemKhachHang] [varchar](20) NULL,
	[soDienThoai] [varchar](11) NULL,
	[diaChi_1] [varchar](50) NULL,
	[diaChi_2] [nchar](10) NULL,
	[loaiKhachHang] [char](3) NULL,
	[tenCongTy] [varchar](40) NULL,
	[ngheNghiep] [varchar](30) NULL,
	[chucVu] [varchar](25) NULL,
	[mucLuong] [int] NULL,
 CONSTRAINT [PK_khachHang] PRIMARY KEY CLUSTERED 
(
	[maKhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[loaiThe]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loaiThe](
	[maLoaiThe] [char](10) NOT NULL,
	[tenLoaiThe] [varchar](10) NULL,
	[moTaLoaiThe] [varchar](50) NULL,
 CONSTRAINT [PK_loaiThe] PRIMARY KEY CLUSTERED 
(
	[maLoaiThe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[taiKhoan]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[taiKhoan](
	[soTaiKhoan] [varchar](20) NOT NULL,
	[maKhachHang] [varchar](12) NULL,
	[soDu] [int] NULL,
	[trangThai] [char](1) NULL,
	[ngayKichHoat] [date] NULL,
	[maPin] [varbinary](50) NULL,
	[tenTaiKhoan] [varchar](50) NULL,
	[matKhauE_Banking] [varbinary](max) NULL,
	[LoaiTien] [varchar](50) NULL,
 CONSTRAINT [PK_taiKhoan] PRIMARY KEY CLUSTERED 
(
	[soTaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[The]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[The](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[soThe] [varbinary](max) NULL,
	[soTaiKhoan] [varchar](20) NULL,
	[ngayLapThe] [date] NULL,
	[ngayHetHan] [date] NULL,
	[tinhTrangThe] [char](1) NULL,
	[loaiThe] [char](10) NULL,
	[donViPhatHanh] [varchar](15) NULL,
 CONSTRAINT [PK_The] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[theCredit]    Script Date: 12/21/2013 10:40:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[theCredit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[soTheCredit] [varbinary](max) NULL,
	[soTaiKhoan] [varchar](20) NULL,
	[ngayLapThe] [date] NULL,
	[ngayHetHan] [date] NULL,
	[hanMucTinDung] [int] NULL,
	[trangThai] [char](1) NULL,
	[maBieuPhi] [varchar](12) NULL,
	[noThemNganHang] [int] NULL,
 CONSTRAINT [PK_theCredit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[bieuPhi]  WITH CHECK ADD  CONSTRAINT [FK_bieuPhi_loaiThe] FOREIGN KEY([maLoaiThe])
REFERENCES [dbo].[loaiThe] ([maLoaiThe])
GO
ALTER TABLE [dbo].[bieuPhi] CHECK CONSTRAINT [FK_bieuPhi_loaiThe]
GO
ALTER TABLE [dbo].[giaoDich]  WITH CHECK ADD  CONSTRAINT [FK_giaoDich_bieuPhi] FOREIGN KEY([maBieuPhi])
REFERENCES [dbo].[bieuPhi] ([maBieuPhi])
GO
ALTER TABLE [dbo].[giaoDich] CHECK CONSTRAINT [FK_giaoDich_bieuPhi]
GO
ALTER TABLE [dbo].[giaoDich]  WITH CHECK ADD  CONSTRAINT [FK_giaoDich_taiKhoan] FOREIGN KEY([soTaiKhoanGui])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[giaoDich] CHECK CONSTRAINT [FK_giaoDich_taiKhoan]
GO
ALTER TABLE [dbo].[taiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_taiKhoan_khachHang] FOREIGN KEY([maKhachHang])
REFERENCES [dbo].[khachHang] ([maKhachHang])
GO
ALTER TABLE [dbo].[taiKhoan] CHECK CONSTRAINT [FK_taiKhoan_khachHang]
GO
ALTER TABLE [dbo].[The]  WITH CHECK ADD  CONSTRAINT [FK_The_loaiThe] FOREIGN KEY([loaiThe])
REFERENCES [dbo].[loaiThe] ([maLoaiThe])
GO
ALTER TABLE [dbo].[The] CHECK CONSTRAINT [FK_The_loaiThe]
GO
ALTER TABLE [dbo].[The]  WITH CHECK ADD  CONSTRAINT [FK_The_taiKhoan] FOREIGN KEY([soTaiKhoan])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[The] CHECK CONSTRAINT [FK_The_taiKhoan]
GO
ALTER TABLE [dbo].[theCredit]  WITH CHECK ADD  CONSTRAINT [FK_theCredit_bieuPhi] FOREIGN KEY([maBieuPhi])
REFERENCES [dbo].[bieuPhi] ([maBieuPhi])
GO
ALTER TABLE [dbo].[theCredit] CHECK CONSTRAINT [FK_theCredit_bieuPhi]
GO
ALTER TABLE [dbo].[theCredit]  WITH CHECK ADD  CONSTRAINT [FK_theCredit_taiKhoan] FOREIGN KEY([soTaiKhoan])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[theCredit] CHECK CONSTRAINT [FK_theCredit_taiKhoan]
GO
