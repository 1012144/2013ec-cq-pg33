USE [E_Banking]
GO
/****** Object:  Table [dbo].[bieuPhi]    Script Date: 11/9/2013 11:48:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bieuPhi](
	[maBieuPhi] [varchar](12) NOT NULL,
	[giaTriToiThieu] [int] NULL,
	[phiGiaoDich] [float] NULL,
	[maLoaiThe] [char](10) NULL,
 CONSTRAINT [PK_bieuPhi] PRIMARY KEY CLUSTERED 
(
	[maBieuPhi] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[giaoDich]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[giaoDich](
	[maGiaoDich] [varchar](12) NOT NULL,
	[soTaiKhoanGui] [varchar](20) NULL,
	[soTaiKhoanNhan] [varchar](20) NULL,
	[maBieuPhi] [varchar](12) NULL,
	[thoiGianGiaoDich] [date] NULL,
	[giaTriGiaoDich] [int] NULL,
	[loaiGiaoDich] [varchar](20) NULL,
	[nguoiGui] [varchar](50) NULL,
	[nguoiNhan] [varchar](50) NULL,
	[nganHangGui] [varchar](50) NULL,
	[nganHangNhan] [varchar](50) NULL,
 CONSTRAINT [PK_giaoDich] PRIMARY KEY CLUSTERED 
(
	[maGiaoDich] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[khachHang]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[khachHang](
	[maKhachHang] [varchar](12) NOT NULL,
	[tenKhachHang] [varchar](10) NULL,
	[hoKhachHang] [varchar](10) NULL,
	[tenDemKhachHang] [varchar](20) NULL,
	[soDienThoai] [varchar](11) NULL,
	[diaChi_1] [varchar](50) NULL,
	[diaChi_2] [nchar](10) NULL,
	[loaiKhachHang] [char](3) NULL,
	[tenCongTy] [varchar](40) NULL,
	[ngheNghiep] [varchar](30) NULL,
	[chucVu] [varchar](25) NULL,
	[mucLuong] [int] NULL,
 CONSTRAINT [PK_khachHang] PRIMARY KEY CLUSTERED 
(
	[maKhachHang] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[loaiThe]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[loaiThe](
	[maLoaiThe] [char](10) NOT NULL,
	[tenLoaiThe] [varchar](10) NULL,
	[moTaLoaiThe] [varchar](50) NULL,
 CONSTRAINT [PK_loaiThe] PRIMARY KEY CLUSTERED 
(
	[maLoaiThe] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[taiKhoan]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[taiKhoan](
	[soTaiKhoan] [varchar](20) NOT NULL,
	[maKhachHang] [varchar](12) NULL,
	[soDu] [int] NULL,
	[trangThai] [char](1) NULL,
	[ngayKichHoat] [date] NULL,
	[maPin] [varbinary](50) NULL,
	[matKhauE_Banking] [varchar](MAX) NULL,
 CONSTRAINT [PK_taiKhoan] PRIMARY KEY CLUSTERED 
(
	[soTaiKhoan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[The]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[The](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[soThe] [varbinary](max) NULL,
	[soTaiKhoan] [varchar](20) NULL,
	[ngayLapThe] [date] NULL,
	[ngayHetHan] [date] NULL,
	[tinhTrangThe] [char](1) NULL,
	[loaiThe] [char](10) NULL,
	[donViPhatHanh] [varchar](15) NULL,
 CONSTRAINT [PK_The] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[theCredit]    Script Date: 11/9/2013 11:48:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[theCredit](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[soTheCredit] [varbinary](max) NULL,
	[soTaiKhoan] [varchar](20) NULL,
	[ngayLapThe] [date] NULL,
	[ngayHetHan] [date] NULL,
	[hanMucTinDung] [int] NULL,
	[trangThai] [char](1) NULL,
	[maBieuPhi] [varchar](12) NULL,
	[noThemNganHang] [int] NULL,
 CONSTRAINT [PK_theCredit] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[bieuPhi]  WITH CHECK ADD  CONSTRAINT [FK_bieuPhi_loaiThe] FOREIGN KEY([maLoaiThe])
REFERENCES [dbo].[loaiThe] ([maLoaiThe])
GO
ALTER TABLE [dbo].[bieuPhi] CHECK CONSTRAINT [FK_bieuPhi_loaiThe]
GO
ALTER TABLE [dbo].[giaoDich]  WITH CHECK ADD  CONSTRAINT [FK_giaoDich_bieuPhi] FOREIGN KEY([maBieuPhi])
REFERENCES [dbo].[bieuPhi] ([maBieuPhi])
GO
ALTER TABLE [dbo].[giaoDich] CHECK CONSTRAINT [FK_giaoDich_bieuPhi]
GO
ALTER TABLE [dbo].[giaoDich]  WITH CHECK ADD  CONSTRAINT [FK_giaoDich_taiKhoan] FOREIGN KEY([soTaiKhoanGui])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[giaoDich] CHECK CONSTRAINT [FK_giaoDich_taiKhoan]
GO
ALTER TABLE [dbo].[taiKhoan]  WITH CHECK ADD  CONSTRAINT [FK_taiKhoan_khachHang] FOREIGN KEY([maKhachHang])
REFERENCES [dbo].[khachHang] ([maKhachHang])
GO
ALTER TABLE [dbo].[taiKhoan] CHECK CONSTRAINT [FK_taiKhoan_khachHang]
GO
ALTER TABLE [dbo].[The]  WITH CHECK ADD  CONSTRAINT [FK_The_loaiThe] FOREIGN KEY([loaiThe])
REFERENCES [dbo].[loaiThe] ([maLoaiThe])
GO
ALTER TABLE [dbo].[The] CHECK CONSTRAINT [FK_The_loaiThe]
GO
ALTER TABLE [dbo].[The]  WITH CHECK ADD  CONSTRAINT [FK_The_taiKhoan] FOREIGN KEY([soTaiKhoan])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[The] CHECK CONSTRAINT [FK_The_taiKhoan]
GO
ALTER TABLE [dbo].[theCredit]  WITH CHECK ADD  CONSTRAINT [FK_theCredit_bieuPhi] FOREIGN KEY([maBieuPhi])
REFERENCES [dbo].[bieuPhi] ([maBieuPhi])
GO
ALTER TABLE [dbo].[theCredit] CHECK CONSTRAINT [FK_theCredit_bieuPhi]
GO
ALTER TABLE [dbo].[theCredit]  WITH CHECK ADD  CONSTRAINT [FK_theCredit_taiKhoan] FOREIGN KEY([soTaiKhoan])
REFERENCES [dbo].[taiKhoan] ([soTaiKhoan])
GO
ALTER TABLE [dbo].[theCredit] CHECK CONSTRAINT [FK_theCredit_taiKhoan]
GO
